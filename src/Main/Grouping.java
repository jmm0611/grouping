package Main;

import java.util.ArrayList;

public class Grouping {

	private ArrayList<Integer> groups;
	private ArrayList<int[]> groupsElem;

	Grouping() {
	}

	public int group(int[] numList, int nGroups) {

		if (nGroups > numList.length) {
			System.out.println("The number of groups is bigger than the number of elements");
			return -1;
		}

		int[][] listGroups = new int[numList.length][2];
		groups = new ArrayList<Integer>();
		groupsElem = new ArrayList<int[]>();

		for (int i = 0; i < numList.length; i++) {
			listGroups[i][0] = numList[i];
			listGroups[i][1] = i;
			groups.add(i);
		}

		while (groups.size() != nGroups) {
			listGroups = checkGroups(listGroups);
		}

		int[] points;
		for (int i = 0; i < groups.size(); i++) {
			points = getClusterPoints(groups.get(i), listGroups);
			groupsElem.add(points);
		}

		for (int i = 0; i < groupsElem.size(); i++) {
			System.out.println("Group " + i);
			for (int j = 0; j < groupsElem.get(i).length; j++)
				System.out.println("(" + groupsElem.get(i)[j] + ")");
		}

		return 0;
	}

	private int[][] checkGroups(int[][] listGroups) {
		int orig = -1, dest = -1;
		double bestDistance = 1000;
		double distance;
		for (int i = 0; i < groups.size(); i++) {
			for (int j = 1; j < groups.size(); j++) {
				if (groups.get(i) != groups.get(j)) {
					distance = calcDistance(groups.get(i), groups.get(j), listGroups);
					if (distance < bestDistance) {
						orig = groups.get(i);
						dest = groups.get(j);
						bestDistance = distance;
					}
				}
			}
		}

		for (int i = 0; i < listGroups.length; i++) {
			if (listGroups[i][1] == dest) {
				listGroups[i][1] = orig;
			}
		}

		for (int i = 0; i < groups.size(); i++) {
			if (groups.get(i) == dest) {
				groups.remove(i);
				break;
			}
		}
		return listGroups;
	}

	private double calcDistance(int cluster1, int cluster2, int[][] listGroups) {
		int[] pointsC1 = getClusterPoints(cluster1, listGroups);
		int[] pointsC2 = getClusterPoints(cluster2, listGroups);

		return minDistance(pointsC1, pointsC2);

	}

	private double minDistance(int[] pointsC1, int[] pointsC2) {
		double bestDistance = 10000;
		double distance;
		for (int i = 0; i < pointsC1.length; i++) {
			for (int j = 0; j < pointsC2.length; j++) {
				distance = Math.abs(pointsC1[i] - pointsC2[j]);
				if (distance < bestDistance)
					bestDistance = distance;
			}
		}
		return bestDistance;
	}

	private int[] getClusterPoints(int cluster, int[][] listGroups) {
		int count = 0;
		for (int i = 0; i < listGroups.length; i++)
			if (listGroups[i][1] == cluster)
				count++;
		int[] points = new int[count];
		int ind = 0;
		for (int i = 0; i < listGroups.length; i++) {
			if (listGroups[i][1] == cluster) {
				points[ind] = listGroups[i][0];
				ind++;
			}
		}
		return points;
	}

	public ArrayList<Integer> getGroups() {
		return groups;
	}

	public ArrayList<int[]> getGroupElements() {
		return groupsElem;
	}
}
