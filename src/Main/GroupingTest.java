package Main;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import Main.Grouping;

public class GroupingTest {

	Grouping x = new Grouping();

	@Test
	public void testDismensions() {
		int[] list = { 1, 2, 5, 7 };
		assertEquals(-1, x.group(list, 5));
		assertEquals(0, x.group(list, 2));
	}

	@Test
	public void testNumberOfGroups() {
		int[] list = { 1, 2, 5, 7 };
		x.group(list, 2);
		assertEquals(2, x.getGroups().size());
		x.group(list, 4);
		assertEquals(4, x.getGroups().size());
	}

	@Test
	public void testDistance() {
		// TEST EXAMPLE 1
		int[] listEx1 = { 16, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23,
				67, 23, 67 };
		x.group(listEx1, 3);
		ArrayList<int[]> groups = x.getGroupElements();

		distanceGroups(groups);

		// TEST EXAMPLE 2
		int[] listEx2 = { 16, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23,
				67, 23, 670 };
		x.group(listEx2, 3);
		groups = x.getGroupElements();

		distanceGroups(groups);

		// TEST EXAMPLE 3
		int[] listEx3 = { 160, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23,
				67, 23, 670 };
		x.group(listEx3, 4);
		groups = x.getGroupElements();

		distanceGroups(groups);

		// TEST EXAMPLE 4
		int[] listEx4 = { 16, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23,
				67, 23, 67 };
		x.group(listEx4, 5);
		groups = x.getGroupElements();

		distanceGroups(groups);
	}

	private void distanceGroups(ArrayList<int[]> groups) {
		for (int i = 0; i < groups.size() - 1; i++) {
			int[] group = groups.get(i);
			int maxElemen = findMax(group);
			int maxDistInGroup = calcMaxDistance(group);

			for (int j = i + 1; j < groups.size(); j++) {
				int[] group2 = groups.get(j);
				int minElem = findMin(group2);

				int groupDist = Math.abs(minElem - maxElemen);
				assertTrue(maxDistInGroup < groupDist);
			}
		}
	}

	private int findMax(int[] group) {
		Arrays.sort(group);
		return group[group.length - 1];
	}

	private int findMin(int[] group) {
		Arrays.sort(group);
		return group[0];
	}

	private int calcMaxDistance(int[] group) {
		Arrays.sort(group);
		int maxDistance = 0;
		for (int i = 0; i < group.length - 1; i++) {
			int newDistance = Math.abs(group[i] - group[i + 1]);
			if (newDistance > maxDistance)
				maxDistance = newDistance;
		}

		return maxDistance;
	}

}
